class AddJoinToUsersAndChannels < ActiveRecord::Migration
  def change
    remove_column :channels, :user_id, :integer
    create_table :channels_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :channel, index: true
    end
  end
end
