# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'edify'
set :name, "keith/#{fetch(:application)}"

set :scm, :git
set :format, :pretty
set :deploy_user, :keith
# set :rbenv_type, :user # or :system, depends on your rbenv setup
# set :rbenv_ruby, "2.1.2"
# set :rbenv_prefix, "RBENV_ROOT=#{fetch(:rbenv_path)} RBENV_VERSION=#{fetch(:rbenv_ruby)} #{fetch(:rbenv_path)}/bin/rbenv exec"
# set :rbenv_map_bins, %w{rake gem bundle ruby rails}
set :rbenv_roles, :all # default value
set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
set :keep_releases, 5

desc 'Start Docker Container'
task :docker_start do
  on roles(:web), in: :sequence do
    within("#{current_path}") do
      execute "sudo", "docker-compose", "stop"
      execute "sudo", "docker-compose", "build"
      execute "sudo", "docker-compose", "up", "-d"
    end
  end
end

desc 'Stop Docker Container'
task :docker_stop do
  on roles(:web), in: :sequence  do
    within("#{current_path}") do
      execute "sudo", "docker-compose", "stop"
    end
  end
end

desc 'Restart Docker Container'
task :docker_stop do
  on roles(:web), in: :sequence  do
    within("#{current_path}") do
      execute "sudo", "docker-compose", "restart"
    end
  end
end

desc 'Build the docker Container'
task :build do
  on roles(:web) do
    within("#{current_path}") do
      execute "docker", "build", "-t", "#{fetch(:name)}" 
    end
  end
end