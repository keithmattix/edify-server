class User < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User
  devise :database_authenticatable, :registerable,
            :recoverable, :trackable, :validatable, :omniauthable
  before_create :skip_confirmation!
  has_many :stories, dependent: :destroy
  has_many :messages
  has_and_belongs_to_many :subscribed_channels, class_name: 'Channel'
  acts_as_voter
end
