class Story < ActiveRecord::Base
  belongs_to :user
  has_attached_file :image, storage: :dropbox, dropbox_credentials: Rails.root.join("config/dropbox.yml"),
      dropbox_options: {}, :styles => { :medium => "600x400>"}
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  acts_as_votable
end
