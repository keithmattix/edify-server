class StorySerializer < ActiveModel::Serializer
  attributes :id, :text, :image
  has_one :user
  has_many :votes_for

end
