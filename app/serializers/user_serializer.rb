class UserSerializer < ActiveModel::Serializer
  attributes :id, :email
  has_many :subscribed_channels
end
