class StoriesController < ApplicationController
  def index
    @stories = Story.order('created_at DESC').all
    render json: @stories, include: { votes_for: { only: :voter_id } }
  end

  def show
    @story = Story.find(params[:id])
    render json: @story
  end

  def default_serializer_options
    {root: false}
  end

  def create
    @story = current_user.stories.build(stories_params)
    if !params[:image_params].nil? and !params[:image_params].empty?
      image_file_name = params[:image_params][:original_filename]
      image_file_name.gsub!(/[^0-9A-Za-z]/, '') 
      image_file_name << ".jpg"
      @story.image_file_name = image_file_name
    end
    if @story.save
      render json: @story
    else
      render json: { errors: @story.errors.full_messages }, status: 422
    end
  end

  def like
    if current_user
      @story = Story.find(params[:id])
      current_user.likes @story
      render json: @story
    end
  end

  def unlike
    if current_user
      @story = Story.find(params[:id])
      current_user.unlike @story
      render json: @story
    end
  end

  private
  def stories_params
    params.require(:story).permit(:text, :image).permit!
  end
end
