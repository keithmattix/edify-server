class PhotosController < ApplicationController
  def search_flickr
    query = photo_params[:query]
    page  = photo_params[:page]
    page ||= 1
    if query == ""
      photos = flickr.photos.getRecent(per_page: 30, page: page)
    else
      photos = flickr.photos.search(text: query, license: "1,2,4,5,7", per_page: 30, page: page)
    end
    pages = photos.pages
    render json: { pages: pages, photos: photos.map { |photo| FlickRaw.url_q(photo) } }
  end

private
  def photo_params
    params.require(:photo).permit(:query, :page)
  end
end