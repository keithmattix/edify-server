class ChatController < ApplicationController
  def index
    @messages = Channel.find_by(name: params[:channel_name]).messages
    render json: @messages
  end

  def send_message
    @message = Channel.find_by(name: params[:message][:channel]).messages.build(chat_params.except(:channel, :socket_id))
    if @message.save
      Pusher.trigger(params[:message][:channel], 'messageSent', @message, {socket_id: params[:message][:socket_id]})
      render json: @message
    else
      render text: "Server Error", status: "500"
    end
  end

  def default_serializer_options
    {root: false}
  end

  def authenticate
    if current_user
      response = Pusher[params[:channel_name]].authenticate(params[:socket_id], {
        user_id: current_user.id,
        user_info: {
          email: current_user.email,
          id: current_user.id
        }
      })
      render json: response
    else
      render text: "Forbidden", status: "403"
    end
  end

  private
  def chat_params
    params.require(:message).permit(:sender_id, :recipient_id, :body, :channel, :socket_id)
  end
end
