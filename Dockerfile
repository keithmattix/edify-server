FROM ubuntu:14.04
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

ENV HOME /root
RUN apt-get update

RUN apt-get install -y build-essential git libssl-dev curl libreadline-dev sqlite3 libsqlite3-dev imagemagick

# Get postgres on here
RUN apt-get install -y libpq-dev

# for nokogiri
RUN apt-get install -y libxml2-dev libxslt1-dev

# for a JS runtime
RUN apt-get install -y nodejs

#
# Install rbenv
RUN git clone https://github.com/sstephenson/rbenv.git /usr/local/rbenv
RUN echo '# rbenv setup' > /etc/profile.d/rbenv.sh
RUN echo 'export RBENV_ROOT=/usr/local/rbenv' >> /etc/profile.d/rbenv.sh
RUN echo 'export PATH="$RBENV_ROOT/bin:$PATH"' >> /etc/profile.d/rbenv.sh
RUN echo 'eval "$(rbenv init -)"' >> /etc/profile.d/rbenv.sh
RUN chmod +x /etc/profile.d/rbenv.sh

# install ruby-build
RUN mkdir /usr/local/rbenv/plugins
RUN git clone https://github.com/sstephenson/ruby-build.git /usr/local/rbenv/plugins/ruby-build


ENV RBENV_ROOT /usr/local/rbenv
ENV PATH $RBENV_ROOT/bin:$RBENV_ROOT/shims:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Install ruby 2.1.2
RUN rbenv install 2.1.2
RUN rbenv global 2.1.2

# Install rubygems
WORKDIR /root
RUN curl http://production.cf.rubygems.org/rubygems/rubygems-2.4.8.tgz | tar xvz && cd rubygems-2.4.8 && ruby setup.rb

# Install Bundler
RUN gem update --system
RUN gem install bundler foreman pg rails thin --no-rdoc --no-ri


# Create project directory
RUN mkdir /edify

# Gemfile caching (prevents bunde install from running on every build)
WORKDIR /tmp
COPY Gemfile Gemfile
COPY Gemfile.lock Gemfile.lock
RUN bundle install

ADD . /edify
WORKDIR /edify
RUN RAILS_ENV=development bundle exec rake assets:precompile --trace
